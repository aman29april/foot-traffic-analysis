class Room
  def initialize(id)
    @visits = []
    @id = id
  end

  attr_reader :id

  # returns total visitors in the room
  def total_visitors
    @visits.length
  end

  def add_visit(visit)
    @visits.push visit
  end

  # total time spend in the room
  def total_time_spend
    @visits.inject(1){|result, elem| elem.time_spend + result}
  end

  # rounded average time spend in room
  def average_time_spend
    (total_time_spend/total_visitors).floor
  end

  def stat_str
    "Room #{id}, #{average_time_spend} minute average visit, #{total_visitors} visitor total"
  end
end

class Visitor
  def initialize(id)
    @id = id
  end
end

class Visit
  def initialize(room, visitor)
    @room = room
    @visitor = visitor
  end

  attr_accessor :exit_time, :entry_time

  def time_spend
    if !( exit_time.nil? || entry_time.nil?)
      (exit_time - entry_time)
    else
      0
    end
  end
end


# method that will read console input, parse it and return rooms array
def parse_logs
  # n is number of input log lines
  n = gets.chomp
  rooms = {}
  visitors = {}
  visits = {}
  n = n.to_i

  for i in 1..n
    input = gets.chomp
    input_ar = input.split(" ")
    visitor_id = input_ar[0].to_i
    room_id = input_ar[1].to_i
    type = input_ar[2]
    minutes = input_ar[3].to_i

    # create room if not present in rooms array
    room = rooms[room_id]
    if room.nil?
      room = Room.new(room_id)
      rooms[room_id] = room
    end

    # create visitor if not present in visitors array
    visitor = visitors[visitor_id]
    if visitor.nil?
      visitor = Visitor.new(visitor_id)
      visitors[room_id] = visitor
    end

    # visit doesnt have any index or key. So we can take visitor id and room id spaced by any delimitor as a key
    visit_key = visitor_id.to_s + " " + room_id.to_s
    visit = visits[visit_key]
    if visit.nil?
      visit = Visit.new(rooms[room_id], visitors[visitor_id])
      room.add_visit(visit)
      visits[visit_key] = visit
    end

    # if person enters the room, put minutes in entry.
    # if person exists room, put minutes in exit time
    if type == "I"
      visit.entry_time = minutes
    elsif type == "O"
      visit.exit_time = minutes
    end

  end

  rooms
end

# start of the program
rooms = parse_logs()

# sort rooms by index and print room stats
rooms.sort.each do |id, room|
  puts room.stat_str
end
